# Fiche Prof

**Thématique :** Les Arbres Binaires de Recherche

**Notions liées :** Les Arbres Binaires, l'algorithmique, la programmation.

**Résumé de l’activité :** L'activité est une suite d'exercices progressifs allant de la découverte et manipulation d'ABR, à la programmation d'une fonction de recherche et insertion.

**Objectifs :** Cette activité permet la prise en main et la manipulation des ABR. Le principe est qu'à la fin de l'activité, les élèves puissent être en mesure de comprendre le fonctionnement d'un ABR, et ce qui en découle tel que l'équilibrage, la recherche et l'insertion.
Exercices progressifs; l'écriture, la réécriture, et la modification d'ABR dans ces exercices vont favoriser la compréhension et l'acquisition de ces compétences.

**Auteur :** MORVRANGE Maxime

**Durée de l’activité :** 1h

**Forme de participation :** Les élèves peuvent être en binôme, et font les exercices en autonomie.

**Matériel nécessaire :** Un crayon, du papier

**Préparation :** Cours sur les ABR, cours sur les Arbres Binaires, TP sur les Arbres Binaires

**Autres références :** Un cours préalable sur les ABR, après un cours sur les Arbres Binaires et des TP de manipulation dessus.
